#### CM 1 (Lundi 23 Septembre 2019)
---

- Présentation du cours et son déroulement. 
- Cours sur le DNS [ici](https://gitlab.com/M3105/cours/blob/master/files/M3105_DNS.pdf)
- Cours de Mr Guégan [ici](https://gitlab.com/M3105/cours/blob/master/files/C-DNS_cours_tpx_tdx-fop.pdf)



#### TD 1 (Mardi 24 Septembre 2019)
---
- Prise en main de vagrant (installation sur les machines du département) [ici](https://gitlab.com/M3105/td/blob/master/README.md)
- Utilisation de l'outils `dig`, voir le [TD0](https://gitlab.com/M3105/td/blob/master/TD0.md)

#### TP 1 (Mercredi 25 et Vendredi 27 Septembre 2019)
---
- Installation d'un serveur DNS voir le sujet du [TD1/TP1](https://gitlab.com/M3105/td/blob/master/TD1.md)
- Compte rendu, évaluation par les pairs. Les instructions sont [ici](https://gitlab.com/M3105/td/blob/master/RAPPORT.md) le rendu se fait sur [moodle](http://moodle.univ-reunion.fr)

#### CM 2 (Jeudi 10 Octobre 2019)
---
- Introduction aux hacking éthique, le cours est [ici](https://gitlab.com/M3105/cours/raw/master/files/Offensive_Security.pdf?inline=false)
- L'approche des services et du réseau par l'attaque

#### TD 2 (Samedi 12 Octobre 2019)
---
- QCM sur le DNS sur [moodle](http://moodle.univ-reunion.fr)
- Suite TP sur le DNS  [TD1/TP1](https://gitlab.com/M3105/td/blob/master/TD1.md)

#### TP 2 (Semaine du 21 Octobre 2019)
---
- ~~Mise en place d'une attaque sur le DNS (spoofing et/ou cache poisoning et/ou pharming )~~
- Suite TP sur le DNS  [TD1/TP1](https://gitlab.com/M3105/td/blob/master/TD1.md), rédaction du rapport. Les instructions sont [ici](https://gitlab.com/M3105/td/blob/master/RAPPORT.md) 
- Ethical hacking - [TP](https://gitlab.com/M3105/tp/blob/master/TP1.md)
- Compte rendu, évaluation par les pairs


#### TD 3 (Mardi 05 Novembre 2019)
---
- QCM sur le hacking éthique sur [moodle](http://moodle.univ-reunion.fr)
- Suite du [TP](https://gitlab.com/M3105/tp/blob/master/TP1.md) sur le hacking éthique
    - SQL injection (peut être que je corrigerai directement cet aspect... à voir)
    - FTP 
    - IRC
- Dans la session d'aujourd'hui, plusieurs notions importantes: reverse shell, backdoor, l'outil netcat (`nc`)

#### TP 3 (Semaine du 21 Octobre 2019) 
---
- Compte rendu, évaluation par les pairs
- Suite du [TP](https://gitlab.com/M3105/tp/blob/master/TP1.md) sur le chacking éthique.
- Explication sur le reverse shell


#### TP 4 (Lundi 28 et Mardi 29 Octobre 2019)
---
- Compte rendu, évaluation par les pairs
- QCM hacking éthique
- [TP2](https://gitlab.com/M3105/tp/blob/master/TP2.md) : Retour vers le DNS / Une approche complète.

#### CM 3 (Mardi 12 Novembre 2019)
---
- Examen ou QCM global
    - Revoir la syntaxe des fichiers de configuration DNS
    - Questions classiques surle DNS et son organisation

#### TP 5 (Vendredi 15 Novembre 2019)
---
- Suite TP2 sur le DNS complet [TP2](https://gitlab.com/M3105/tp/blob/master/TP2.md)
- Ecriture de script de déploiement pour `imunes`.

#### TP 6 (Mardi 19 et Vendredi 22 Novembre 2019)
---
- Suite TP2 sur le DNS complet [TP2](https://gitlab.com/M3105/tp/blob/master/TP2.md)
- Finir la rédaction du rapport. La grille de notation est mise en ligne avec le sujet
- Finir la partie aller plus loin du TP2.


