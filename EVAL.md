## Evaluation par les pairs


#### Notes
---

- Consignes exhaustives mais brèves.
    - Sujet / consigne pour le travail
    - Déroulement de la tâche
    - Instruction technique
    - Exemple
        - Consignes pour le rendu
            - Présenter sous forme de problème / et voir comment le problème est résolu
                - Quel est le problème ? 
                - Comment avez vous fait pour résoudre le problème
            - déposer un document
                - Exposé clair du problème
                - Exposé claire de la solution, et explication détaillée de la solution et de l'approche
                - Exposé pas à pas de l'implémentation de la solution (reproductibilité)
            - déposer une vidéo
            - déposer le code source / script / fichier de configuration
        - Consignes pour l'évaluation. Il faut expliqué avec un feedback clairement ce qui manque, ou autre
            - Est-ce que la technique proposée est correcte et permet d'atteindre l'objectif ?
            - Est-ce que les instructions sont facilement reproductible ? 
            - Est-ce que le rapport est de bonne qualité
                - Pas de fautes d'orthographes 
                - Le rapport est bien structuré
                - L'histoire est cohérente et progressive
            - Est-ce que les outils utilisés sont corrects et adaptés ?
                - les outils correspondent aux besoins/nécessités
            - Est ce que la/les solution(s) est originale(s) ?
            - Est ce que les fichiers sont propres, commentés et compréhensible ? 
- Les phases:
    - Phase Mise en place
    - Phase de remise
    - Phase d'évaluation
    - Phase de notation des évaluations
- Possibilité
    - Plusieurs évaluations par personnes
    - Plusieurs évaluations par rendu
    - Possiblité de donner une note par rapport à l'évaluation